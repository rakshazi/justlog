# gitlab.com/rakshazi/justlog

Simple web interface for docker logs.
That tool connects to docker socket, reads logs and exposes them on HTTP.

**Usage**

```bash
docker run -v /var/run/docker.sock:/var/run/docker.sock:ro -p 8080:8080 registry.gitlab.com/rakshazi/justlog
```
