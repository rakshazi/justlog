package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/tv42/httpunix"
)

type Container struct {
	ID     string `json:"Id"`
	Name   string
	Names  []string `json:"Names"`
	Reader *bufio.Reader
}

var client *http.Client
var containersSlice []*Container

func init() {
	socket := &httpunix.Transport{
		DialTimeout:           100 * time.Millisecond,
		RequestTimeout:        1 * time.Second,
		ResponseHeaderTimeout: 1 * time.Second,
	}
	socket.RegisterLocation("docker", "/var/run/docker.sock")

	client = &http.Client{
		Transport: socket,
	}
}

func main() {
	containersSlice = getContainers()
	http.HandleFunc("/", handleHttp)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}

func writeLogs(container *Container, w http.ResponseWriter) {
	for {
		line, err := container.Reader.ReadBytes('\n')
		if err != nil {
			panic(err)
		}
		// first 8 bytes are part of header, we don't need it here
		text := string(bytes.TrimSpace(line[8:]))
		w.Write([]byte(text + "\n"))
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		} else {
			fmt.Println("Damn, no flush")
		}
	}
}

func handleHttp(w http.ResponseWriter, r *http.Request) {
	name := strings.TrimPrefix(r.URL.Path, "/")
	if name == "" {
		response := "You must put container name to url, eg: https://site.com/api.\n\nAllowed container names:\n"
		for _, container := range containersSlice {
			response = response + container.Name + "\n"
		}
		w.WriteHeader(404)
		w.Write([]byte(response))
	}
	for _, container := range containersSlice {
		if container.Name == name {
			writeLogs(container, w)
		}
	}
}

func getContainers() []*Container {
	resp, err := client.Get("http+unix://docker/containers/json")
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	var containers []*Container
	json.Unmarshal(body, &containers)
	if err != nil {
		panic(err)
	}
	for i, container := range containers {
		//docker-compose container names like project_container_1
		name := strings.Split(container.Names[0], "_")
		if len(name) >= 2 {
			containers[i].Name = name[1]
		} else {
			containers[i].Name = name[0]
		}
		containers[i].Reader = getLogReader(containers[i])
	}
	return containers
}

func getLogReader(container *Container) *bufio.Reader {
	resp, err := client.Get("http+unix://docker/containers/" + container.ID + "/logs?stdout=true&stderr=true&follow=true&tail=100&since=0")
	if err != nil {
		panic(err)
	}
	return bufio.NewReader(resp.Body)
}

func sendLogs(container *Container, reader *bufio.Reader, wg *sync.WaitGroup) {
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			panic(err)
		}
		// first 8 bytes are part of header, we don't need it here
		text := string(bytes.TrimSpace(line[8:]))
		fmt.Println(container.Name, text)
	}
	wg.Done()
}
