FROM golang:alpine AS builder

RUN apk --no-cache add git ca-certificates tzdata && update-ca-certificates && \
    adduser -D -g '' app
WORKDIR /go/src/gitlab.com/rakshazi/justlog
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -v -a -installsuffix cgo -o justlog

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /go/src/gitlab.com/rakshazi/justlog/justlog /bin/justlog

ENTRYPOINT ["/bin/justlog"]
EXPOSE 8080
